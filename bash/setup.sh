# To run this, use the following command in the terminal: bash bash/setup.sh

echo ""
echo ""
echo "[ ⭕️ NOSop3 - Interactive template configurator ]"
echo ""
echo ""


{
  echo -n "0. Systeem naam (voorbeeld: op3/interactive-folder-naam): ";
  echo "";
  while read -r REPLY; do
    if [[ -z "${REPLY}" ]]; then
      echo "🤖: Dit veld is verplicht. Er is niks ingevoerd. Probeer opnieuw:"
      echo "";
    else
      sysName="${REPLY}";
      echo "";
      break
    fi
  done
}

{
  echo -n "1. Auteur: ";
  echo "";
  while read REPLY; do
    if [[ -z "${REPLY}" ]]; then
      echo "🤖: Dit veld is verplicht. Er is niks ingevoerd. Probeer opnieuw:"
      echo "";
    else
      author="${REPLY}";
      echo "";
      break
    fi
  done
}

{
  echo -n "2. Kies een projectnaam voor de interactive: ";
  echo "";
  while read REPLY; do
    if [[ -z "${REPLY}" ]]; then
      echo "🤖: Dit veld is verplicht. Er is niks ingevoerd. Probeer opnieuw:"
      echo "";
    else
      projectname="${REPLY}";
      echo "";
      break
    fi
  done
}
{
  echo -n "3. Kies een naam voor de repo (lowercase en zonder spaties): ";
  echo "";
  while read REPLY; do
    if [[ -z "${REPLY}" ]]; then
      echo "🤖: Dit veld is verplicht. Er is niks ingevoerd. Probeer opnieuw:"
      echo "";
    else
      reponame="${REPLY}";
      echo "";
      break
    fi
  done
}
{
  echo -n "4. Omschrijf het project in één zin: ";
  echo "";
  while read REPLY; do
    if [[ -z "${REPLY}" ]]; then
      echo "🤖: Dit veld is verplicht. Er is niks ingevoerd. Probeer opnieuw:"
      echo "";
    else
      description="${REPLY}";
      echo "";
      break
    fi
  done
}
{
  echo -n "5. Kies een redactionele titel voor de interactive (bijvoorbeeld: Taxeer je zeer): ";
  echo "";
  while read REPLY; do
    if [[ -z "${REPLY}" ]]; then
      echo "🤖: Dit veld is verplicht. Er is niks ingevoerd. Probeer opnieuw:"
      echo "";
    else
      title="${REPLY}";
      echo "";
      break
    fi
  done
}
{
  echo -n "6. Twitter hashtags (voorbeeld: #twitter #hashtag #gescheiden #met #hashtag #en #spaties): ";
  echo "";
  read -e;
  twitterHashtags="${REPLY/,/\\,}";
  echo "";
}
{
  echo -n "7. Enalyzer enquete ID (Maak hier een enquete aan https://www.enalyzer.com/survey/library): ";
  echo "";
  read -e;
  enalyzer="${REPLY}";
  echo "";
}
{
  echo -n "8. Voer een wachtwoord in (Indien niet nodig, druk op enter): ";
  echo "";
  read -e;
  password="${REPLY}";
  if [ -z "$password" ]
    then
      echo "Geen wachtwoord ingevoerd."
  fi
  echo "";
}

echo ""
echo "🎉 Project is gereed! 🎉"
echo "Controleer je wijzingingen in package.json & project-settings.json"
echo ""

# edits package.json
sed -i '' '/"name":/ s/"name":[^,]*/"name": "'"$reponame"'"/' ./package.json
sed -i '' '/"description":/ s/"description":[^,]*/"description": "'"$description"'"/' ./package.json
sed -i '' '/"author":/ s/"author":[^,]*/"author": "'"$author"'"/' ./package.json

# edits project-settings.json

# meta
sed -i '' '/"sysName" :/ s/"sysName" :[^,]*/"sysName" : "'"${sysName//\//\\/}"'"/' ./project-settings.json
sed -i '' '/"name" :/ s/"name" :[^,]*/"name" : "'"$reponame"'"/' ./project-settings.json
sed -i '' '/"title" :/ s/"title" :[^,]*/"title" : "'"$title"'"/' ./project-settings.json
sed -i '' '/"description" :/ s/"description" :[^,]*/"description" : "'"$description"'"/' ./project-settings.json

# twitter hashtags
sed -i '' '/"twitterHashtags" :/ s/"twitterHashtags" :[^,]*/"twitterHashtags" : "'"$twitterHashtags"'"/' ./project-settings.json

# tracking
year=$(date +'%Y')
month=$(date +'%m')
sed -i '' '/"path" :/ s/"path" :[^,]*/"path" : "'"app.op3-$reponame.$year.$month"'"/' ./project-settings.json

# enalyzer
sed -i '' '/"enalyzer" :/ s/"enalyzer" :[^,]*/"enalyzer" : "'"$enalyzer"'"/' ./project-settings.json

# password
{
if [[ ! -z $password ]]
  then
    # not
    sed -i '' '/"phrase" :/ s/"phrase" :[^,]*/"phrase" : "'"$password"'"/' ./project-settings.json
  else
    # empty
    sed -i '' '/"isProtected" :/ s/"isProtected" :[^,]*/"isProtected" : false/' ./project-settings.json
    sed -i '' '/"phrase" :/ s/"phrase" :[^,]*/"phrase" : ""/' ./project-settings.json
fi
}

exit
