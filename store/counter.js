const initialState = () => {
  return {
    counter: 0
  };
};

export const state = () => initialState();

export const getters = {
  getCounter(state) {
    return state.counter;
  }
};

export const actions = {
  setCounter({ commit }, payload) {
    this.$log('set counter 1');
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        commit('SET_COUNTER', payload);
        this.$log('set counter 2');
        resolve();
      }, 2500);
    });
  }
};

export const mutations = {
  SET_COUNTER(state, payload) {
    state.counter = payload;
  }
};
