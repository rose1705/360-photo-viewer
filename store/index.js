const initialState = () => {
  return {
    isAppStarted: false,
    isAccessGranted: false,

    isLoadingActive: false,
    isPanelActive: false,

    isScrollBlocked: false,
    isScrollProgressActive: true,
    scrollDirection: null,

    prismic: {},

    textReplacements: {}
  };
};

export const state = () => initialState();

export const getters = {
  getIsAppStarted(state) {
    return state.isAppStarted;
  },

  getAccessGranted(state) {
    return state.isAccessGranted;
  },

  getTextReplacements(state) {
    return state.textReplacements;
  },

  getLoadingActive(state) {
    return state.isLoadingActive;
  },

  getPanelActive(state) {
    return state.isPanelActive;
  },

  // Scrolling
  getScrollProgressActive(state) {
    return state.isScrollProgressActive;
  },
  getScrollBlocked(state) {
    return state.isScrollBlocked;
  },
  getScrollDirection(state) {
    return state.scrollDirection;
  },

  // Prismic
  getPrismic(state) {
    return state.prismic;
  },

  getPrismicLabelById: (state) => (id) => {
    if (state.prismic.all_labels.find((label) => label.label_id === id)) {
      return state.prismic.all_labels.find((label) => label.label_id === id)
        .label_object;
    } else {
      console.error(`Could not retrieve Prismic label by id: ${id}`); // eslint-disable-line
    }
  },
  getPrismicContentById: (state) => (id) => {
    if (
      state.prismic.all_content.find((content) => content.content_id === id)
    ) {
      return state.prismic.all_content.find(
        (content) => content.content_id === id
      ).content_object;
    } else {
      console.error(`Could not retrieve Prismic content by id: ${id}`); // eslint-disable-line
    }
  },

  getPrismicDefinitionById: (state) => (id) => {
    const foundDefinition = state.prismic.all_definitions.find(
      (content) =>
        content.definition_id.toLowerCase().replace(/[^A-Z0-9]/gi, '') === id
    );
    if (foundDefinition) {
      return {
        id: foundDefinition.definition_id,
        title: foundDefinition.definition_title,
        description: foundDefinition.definition_object
      };
    } else {
      console.error(`Could not retrieve Prismic definition by id: ${id}`); // eslint-disable-line
    }
  },

  getPrismicColofon(state) {
    return state.prismic.colofon;
  },

  getPrismicShareText(state) {
    return state.prismic.share_text;
  },
  getPrismicHashtags(state) {
    return state.prismic.hashtags;
  }
};

export const actions = {
  // DO STUFF WHEN FIRST TIME ENTER

  // ASYNC STUFF
  async nuxtClientInit({ commit }, context) {
    if (localStorage.getItem('accessGranted')) {
      commit('SET_ACCESS_GRANTED');
    }
    try {
      // Prismic
      const prismic = await context.$prismic.api
        .getByUID('interactive_content', 'voorbeeld')
        .then((response) => {
          return response.data;
        });

      commit('SET_PRISMIC', prismic);

      // axios example
      // const url = `${context.env.baseUrl}data/colofon.json`;
      // const content = await axios.get(url).then((response) => {
      //   return response.data.colofon;
      // });
    } catch (e) {
      console.warn('Content from NuxtClientInit could not be retrieved: ', e); // eslint-disable-line
    }
  },

  setAccessGranted: ({ commit }) => {
    commit('SET_ACCESS_GRANTED');
  },

  setIsAppStarted: ({ commit }, payload) => {
    commit('SET_APP_IS_STARTED', payload);
  },

  setTextReplacements: ({ commit }, payload) => {
    commit('SET_TEXT_REPLACEMENTS', payload);
  },

  setLoadingActive: ({ commit }, payload) => {
    commit('SET_LOADING_ACTIVE', payload);
  },

  setPanelActive: ({ commit }, payload) => {
    commit('SET_PANEL_ACTIVE', payload);
  },

  setScrollBlocked: ({ commit }, payload) => {
    commit('SET_SCROLL_BLOCKED', payload);
  },
  setScrollProgressActive: ({ commit }) => {
    commit('SET_SCROLLPROGRESS_ACTIVE');
  },
  setScrollDirection: ({ commit }, payload) => {
    commit('SET_SCROLL_DIRECTION', payload);
  }
};

export const mutations = {
  SET_PRISMIC(state, payload) {
    state.prismic = payload;
  },

  SET_APP_IS_STARTED(state, payload) {
    state.isAppStarted = payload;
  },

  SET_ACCESS_GRANTED(state) {
    state.isAccessGranted = true;
    localStorage.setItem('accessGranted', true);
  },

  SET_TEXT_REPLACEMENTS(state, payload) {
    state.textReplacements = payload;
  },

  SET_LOADING_ACTIVE(state, payload) {
    state.isLoadingActive = payload;
  },

  SET_PANEL_ACTIVE(state, payload) {
    state.isPanelActive = payload;
  },

  SET_SCROLLPROGRESS_ACTIVE(state, payload) {
    state.isScrollProgressActive = payload;
  },
  SET_SCROLL_BLOCKED(state, payload) {
    state.isScrollBlocked = payload;
  },
  SET_SCROLL_DIRECTION(state, payload) {
    state.scrollDirection = payload;
  }
};
