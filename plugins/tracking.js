/* eslint-disable */
import {
  trackPage as libTrackPage,
  trackChapter as libTrackChapter,
  trackClick as libTrackClick
} from '~/lib/track';

import { deploy } from '~/project-settings.json';

const logStyling=[
  'font-weight:bold; background-color: #FF6E6E; color: white; padding: 2px 5px; border-top-left-radius: 2px; border-bottom-left-radius: 2px;',
  'background-color: #ccc;padding 5px; color: #fff; font-weight: bold; padding: 2px 5px; border-top-right-radius: 2px; border-bottom-right-radius: 2px;',
]

export default (context, inject) => {
  const trackClick = (label) => {
    libTrackClick(label);
    if (deploy.showTrackingLogs) {
      console.log(`🔍%cTrackClick:%c${label}`, logStyling[0],logStyling[1]);
    }
  };
  inject('trackClick', trackClick);

  const trackPage = (label) => {
    libTrackPage(label);
    if (deploy.showTrackingLogs) {
      console.log(`📖%cTrackPage:%c${label}`, logStyling[0],logStyling[1]);
    }
  };
  inject('trackPage', trackPage);

  const trackChapter = (label) => {
    libTrackChapter(label);
    if (deploy.showTrackingLogs) {
      console.log(`📄%cTrackChapter:%c${label}`, logStyling[0],logStyling[1]);
    }
  };
  inject('trackChapter', trackChapter);
};
