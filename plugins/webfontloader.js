import WebFont from 'webfontloader';

WebFont.load({
  google: {
    families: ['Barlow:100,300,400,500,700']
  }
});
