/* eslint-disable */
import { deploy } from '~/project-settings.json';

export default (context, inject) => {
  const log = (msg) => (deploy.showLogs ? console.log(`[$log] ${msg}`) : null);
  inject('log', log);
};
