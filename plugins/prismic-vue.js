import Vue from 'vue';
import PrismicVue from 'prismic-vue';
import { prismic } from '../project-settings.json';
import linkResolver from './link-resolver';

Vue.use(PrismicVue, {
  endpoint: prismic.endpoint,
  linkResolver
});
