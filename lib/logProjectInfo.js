import project from '../project-settings.json';
import pkg from '../package.json';


let hasError = false;
// https://stackoverflow.com/questions/9781218/how-to-change-node-jss-console-font-color

const colors = {
  Reset: "\x1b[0m",
  Bright: "\x1b[1m",
  Dim: "\x1b[2m",
  Underscore: "\x1b[4m",
  Blink: "\x1b[5m",
  Reverse: "\x1b[7m",
  Hidden: "\x1b[8m",

  FgBlack: "\x1b[30m",
  FgRed: "\x1b[31m",
  FgGreen: "\x1b[32m",
  FgYellow: "\x1b[33m",
  FgBlue: "\x1b[34m",
  FgMagenta: "\x1b[35m",
  FgCyan: "\x1b[36m",
  FgWhite: "\x1b[37m",

  BgBlack: "\x1b[40m",
  BgRed: "\x1b[41m",
  BgGreen: "\x1b[42m",
  BgYellow: "\x1b[43m",
  BgBlue: "\x1b[44m",
  BgMagenta: "\x1b[45m",
  BgCyan: "\x1b[46m",
  BgWhite: "\x1b[47m",
}

function getStatus( condition=true, title, value, titleSize=25 ){
  const color = condition ? colors.FgRed : colors.FgGreen;
  const icon = condition ? '🔴' : '🟢';

  let newTitle = title + ':';
  while(newTitle.length < titleSize){
    newTitle += ' ';
  }
  return `${color} ${icon} ${newTitle} ${value}`;
}

const projectInfo = [
  {
    condition: pkg.name.includes('op3-nuxt-template'),
    title: 'Package',
    value: pkg.name
  },
  {
    condition: project.meta.sysName.includes('dev-'),
    title: 'Adress',
    value: `https://app.nos.nl/${project.meta.sysName}`
  },
  {
    condition: !project.meta.hasPrettyUrl, 
    title: `Pretty URL`, 
    value: project.meta.hasPrettyUrl ? project.meta.prettyUrl : 'No pretty URL'
  },
  {
    condition: !project.enalyzer,
    title: `Enalyzer URL`, 
    value: !project.enalyzer ? 'No enalyzer set' :`https://surveys.enalyzer.com/?pid=${project.enalyzer}`
  },
  {
    condition: false, 
    title: `Prismic Endpoint`, 
    value: project.prismic.endpoint
  },
  {
    break: true,
  },
  {
    condition: project.meta.title.includes('NOS op 3 Interactive'), 
    title: `Title`, 
    value: project.meta.title
  },
  {
    condition: project.meta.description.includes('Ontdek een interactief verhaal van NOS op 3!'), 
    title: `Description`, 
    value: project.meta.description
  },
  {
    condition: project.meta.twitterHashtags === '#interactive #nosop3', 
    title: `HashTags`,
    value: project.meta.twitterHashtags
  },
  {
    break: true,
  },
  {
    condition: project.deploy.showLogs,
    title:`Showing Logs`,
    value: project.deploy.showLogs ? 'Yes' : 'No'
  },
  {
    condition: project.deploy.showTrackingLogs,
    title: 'Tracking Logs',
    value: project.deploy.showTrackingLogs ? 'Yes' : 'No'
  },
  {
    condition: project.tracking.path.includes('template' || 'nuxt'),
    title: 'Tracking Path',
    value: project.tracking.path
  },
  {
    break: true,
  },
  {
    condition: project.password.isProtected,
    title: 'Password Protected',
    value: project.password.isProtected ? `Locked` : `Unlocked`
  },
  {
    condition: project.password.isProtected,
    title: 'Password Phrase',
    value: project.password.phrase
  },
]

function printLine({text= '',isHeader= false, isFooter= false, length = 120, beforeText='',afterText='' } = {}){
  let line = isHeader ? '╭──' : isFooter ? '╰──' : '│  ';
  line += `${beforeText}${text}\x1b[0m${afterText}`;
  for (let i = 0; i < length - text.length; i++) {
    line += isHeader || isFooter ? '─' : ' '
  }
  line += isHeader ? '╮' : isFooter ? '╯' : '│';
  console.log(line);
}

export default function logProjectInfo() {
  console.log('')
  console.log('')
  
  printLine({
    isHeader: true,
    text: `[ VERSION: ${process.env.NODE_ENV === 'production' ? 'PRODUCTION' : 'DEVELOPMENT'} ]`
  });
  
  printLine();
  
  projectInfo.forEach(item=>{
    !item.break ? printLine({
      afterText: '     ',
      text: getStatus(item.condition,item.title,item.value)}) : printLine()
  })

  printLine();
  printLine({ isFooter: true });
  console.log('');
  console.log('');
  printLine({
    isHeader: true,
    text: `[ DEPLOY CHECKLIST ]`
  });
  printLine();
  project.deployChecklist.forEach((item,index)=>{
    Object.keys(item).forEach(key=>{
      printLine({
        text: getStatus(!project.deployChecklist[index][key], key, project.deployChecklist[index][key] ? 'Yes' : 'No', 35),
        afterText: '     '
      });
    })
  })
  printLine();
  printLine({ isFooter: true });
  console.log('');
  console.log('');
  console.log('[ ASSET LIST ]')
  console.log('');
  console.log(`- iNOS iFrame Link:    https://app.nos.nl/${project.meta.sysName}/link.html` );
  console.log(`- Breaking:            https://app.nos.nl/${project.meta.sysName}/img/breaking.jpg` );
  console.log(`- Social:              https://app.nos.nl/${project.meta.sysName}/img/social.jpg` );
  console.log(`- Index:               https://app.nos.nl/${project.meta.sysName}/img/index_header.jpg` );
  console.log(`- Link NOS website:    https://app.nos.nl/${project.meta.sysName}/img/link_nos_website.jpg`);

  console.log('')
  console.log('')
  console.log('──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────')
  console.log('')
  console.log('')

  if(hasError && process.env.NODE_ENV === 'production'){
    process.exit();
  }
  
}
