/* eslint-disable */

import deepmerge from "deepmerge";
import { tracking } from "../project-settings.json";

const baseName = tracking.path;
const defaultParameters = {
  level2: 1,
  chapter1: "undefined",
  chapter2: "undefined",
  chapter3: "special",
  customVars: {
    site: {
      1: "[site]", // `platform`
      2: "[nos]", // `broadcaster`
      3: "[nosop3]", // `programma`
      4: "[portal]", // `level2 type`
      6: "[article]", // `eventtype`
      7: "[general]", // `mediatype`
      9: "[site]", // `omroep 2 - nos platform`
      10: "[special]", // `omroep 3 - systemtag`
    },
  },
};

// Track page loads
export function trackPage (page) {
  const trackingData = {
    customVars: {
      site: {
        8: `[${page}]`, // `title`
      },
    },
    name: `${baseName}.${page}.open`,
  };

  const trackingParameters = deepmerge(defaultParameters, trackingData);

  // Prevent fatal error if AT Internet script cannot be loaded.
  try {
    tag.page.send(trackingParameters);
  } catch (error) {
    console.error(error);
  }
}

// Track clicks on UI elements
export function trackClick (click) {
  const trackingData = {
    name: `${baseName}.${click}.click`,
    type: "action",
  };

  const trackingParameters = deepmerge(defaultParameters, trackingData);

  // Prevent fatal error if AT Internet script cannot be loaded.
  try {
    tag.click.send(trackingParameters);
  } catch (error) {
    console.error(error);
  }
}

// Track scroll depth through chapters (sections) in viewport
export function trackChapter (chapter) {
  const trackingData = {
    name: `${baseName}.${chapter}`,
    type: "action",
  };

  const trackingParameters = deepmerge(defaultParameters, trackingData);

  // Prevent fatal error if AT Internet script cannot be loaded.
  try {
    tag.page.send(trackingParameters);
  } catch (error) {
    console.error(error);
  }
}
