import { mapGetters, mapActions } from 'vuex'; // vuex store
import gsap from 'gsap';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger.js';

import { password } from '~/project-settings.json';
import { isMSIE } from '~/lib/detect';
gsap.registerPlugin(ScrollTrigger);

export default {
  beforeMount() {
    window.addEventListener('resize', this.resizeHandler);
    this.init();

    if (isMSIE()) {
      this.$router.push({
        path: '/browser-update'
      });
    }

    // TODO:
    // FIX THIS FOR WHEN CHANGING ROUTES MANUALLY
    // CHECK BEFOREROUTEENTER
    if (password.isProtected) {
      if (!this.getAccessGranted) {
        this.$router.push('/wachtwoord');
      }
    }

    // TODO:
    // MOVE THIS TO STORE NUXTCLIENTINIT ?
    // NOT WORKING CORRECTLY WHEN USING MORE LAYOUTS
    if (!this.getIsAppStarted) {
      this.$trackPage('Start');
      this.setIsAppStarted(true);
    }
  },

  mounted() {
    this.$log(this.getCounter);
    this.setCounter(5);
    this.$log(this.getCounter);

    this.setTextReplacements({
      '[oude-text-variabele]': '{nieuwe-text-variabele}'
    });
  },
  computed: {
    ...mapGetters({
      getIsAppStarted: 'getIsAppStarted',
      getAccessGranted: 'getAccessGranted',
      getCounter: 'counter/getCounter'
    })
  },
  methods: {
    ...mapActions({
      setIsAppStarted: 'setIsAppStarted',
      setCounter: 'counter/setCounter',
      setTextReplacements: 'setTextReplacements'
    }),
    init() {
      this.$log('Set css variable vh');
      const vh = window.innerHeight * 0.01;
      document.documentElement.style.setProperty('--vh', `${vh}px`);
    },
    resizeHandler() {
      this.init();
      ScrollTrigger.refresh();
    }
  }
};
