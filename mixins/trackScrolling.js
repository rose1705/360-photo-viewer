import { mapActions } from 'vuex';
import gsap from 'gsap';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger.js';
gsap.registerPlugin(ScrollTrigger);

export default {
  data() {
    return {
      scrollDepth: 0,
      scrollDirection: null,
      viewedDepth: null
    };
  },

  mounted() {
    this.$nextTick(() => {
      ScrollTrigger.create({
        scroller: document.querySelector('.app-inner'),
        onUpdate: (self) => {
          this.scrollDepth =
            self.progress.toFixed(4) > 0.99 ? 1 : self.progress.toFixed(4);
          if (self.direction === 1) {
            const tenPerc = Math.floor(this.scrollDepth * 10);
            if (tenPerc > this.viewedDepth && tenPerc !== this.viewedDepth) {
              this.viewedDepth = tenPerc;
              this.$trackClick(`Viewed: ${this.viewedDepth * 10}%`);
            }
            if (this.scrollDirection !== 'down') {
              this.scrollDirection = 'down';
            }
          } else if (this.scrollDirection !== 'up') {
            this.scrollDirection = 'up';
          }
        }
      });
    });
  },
  watch: {
    scrollDirection(newVal, oldVal) {
      this.setScrollDirection(newVal);
    }
  },
  methods: {
    ...mapActions({
      setScrollDirection: 'setScrollDirection'
    })
  }
};
