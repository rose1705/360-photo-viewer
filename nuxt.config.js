import { meta, deploy, prismic, password, enalyzer } from "./project-settings";
import * as pkg from "./package";
import logProjectInfo from "./lib/logProjectInfo";
import replace from 'replace-in-file';

const deployServer = deploy.isTestEnvironment ? `https://test-app.nos.nl/${meta.sysName}/` : `https://app.nos.nl/${meta.sysName}/`;

export default {
  ssr: false,
  env: {
    baseUrl: (process.env.NODE_ENV === 'production' ? deployServer : `/${meta.sysName}/`),
    
    enalyzer,
    password: password.phrase,
  },

  router: {
    base: `/${meta.sysName}/`,
    mode: 'hash',
  },

  /*
   ** Headers of the page
   */
  head: {
    htmlAttrs: {
      lang: "nl",
      dir: "ltr",
    },
    title: meta.title,
    meta: [
      { charset: "utf-8" },

      // { name: "viewport", content: "height=device-height, width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=3.0, user-scalable=yes, minimal-ui" },
      // prevent user scaling
      { name: "viewport", content: "height=device-height, width=device-width, initial-scale=1.0, user-scalable=no, minimal-ui" },

      { hid: "description", name: "description", content: meta.description },
      { name: "apple-mobile-web-app-capable", content: "yes" },
      { name: "apple-mobile-web-app-status-bar-style", content: "white" },
      { hid: "apple-mobile-web-app-title", name: "apple-mobile-web-app-title", content: meta.title },

      { hid: "og:title", property: "og:title", content: meta.title },
      { hid: "og:description", property: "og:description", content: meta.description },
      { property: "og:type", content: "website" },

      // Pretty URL
      // Vraag een redirect aan bij de NPO van https://www.nosop3.nl/INTERACTIVE_NAAM naar https://app.nos.nl/op3/INTERACTIVE_NAAM.
      // Zet dan de "hasPrettyUrl"-optie op "true" in project-settings.json.

      { hid: "og:url", property: "og:url", content: `${meta.hasPrettyUrl ? meta.prettyUrl : "https://app.nos.nl"}/${meta.sysName}/` },
      { hid: "og:image", property: "og:image", content: `${meta.hasPrettyUrl ? meta.prettyUrl : "https://app.nos.nl"}/${meta.sysName}/img/social.jpg` },

      { hid: "twitter:title", name: "twitter:title", content: meta.title },
      { name: "twitter:card", content: "summary_large_image" },
      { name: "twitter:site", content: "@nosop3" },
      { name: "twitter:creator", content: "@nosop3" },

      { hid: 'robots', name: 'robots', content: deploy.isProd ? 'all': 'noindex, nofollow' }

    ],
    script: [
      // Track naar NPO production of NPO development in AT Internet.
      //
      // Schakel "isProd" in project-settings.json naar `true` kort voor de production build.
      //
      // 596579 - NPO dev
      // 595271 - NPO prod
      {
        type: "text/javascript",
        src: `//tag.aticdn.net/${deploy.isProd ? "595271" : "596579"}/smarttag.js`,
      },
      {
        type: "text/javascript",
        innerHTML: "var tag = new ATInternet.Tracker.Tag();",
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  /*
   ** Polyfills
   */
  polyfill: {
    features: [
      {
        require: "intersection-observer",
        detect: () => "IntersectionObserver" in window,
      },
      {
        require: "mdn-polyfills/Array.from",
        detect: () => "Array.from" in window,
      },
      {
        require: "mdn-polyfills/Array.prototype.includes",
        detect: () => "Array.prototype.includes" in window,
      },
      {
        require: "mdn-polyfills/Element.prototype.classList",
        detect: () => "Element.prototype.classList" in window,
      },
      {
        require: "mdn-polyfills/Element.prototype.closest",
        detect: () => "Element.prototype.closest" in window,
      },
      {
        require: "mdn-polyfills/Object.entries",
        detect: () => "Object.entries" in window,
      },
      {
        require: "mdn-polyfills/Object.values",
        detect: () => "Object.values" in window,
      },
    ],
  },

  /*
   ** Customize the progress-bar color
   */
  loading:{
    color: '#ff6e6e',
  },
  loadingIndicator: {
    name: 'pulse',
    color: '#ff6e6e',
    background: 'white'
  },
  /*
   ** Global CSS
   */
  css: [
    'normalize.css/normalize.css',
    '~/assets/styles/css/main.css',
  ],

  /*
   ** Style Resources
   */
  styleResources: {
    scss: [
      // '~/assets/scss/helper.scss',
      // '~/assets/scss/extends.scss'
      "~/assets/styles/scss/global.scss",
    ],
   },

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: "~/plugins/tracking", ssr: false },
    { src: "~/plugins/log", ssr: false },

    { src: "~/plugins/webfontloader", ssr: false },
    { src: "~/plugins/vue-observe-visibility", ssr: false },
    { src: "~/plugins/vue-clipboard2", ssr: false },
    { src: '~/plugins/client-only.js', mode: 'client' },
    
    // Libraries that can be enabled on app per app basis.
    { src: "~/plugins/prismic-vue", ssr: false },
    { src: "~/plugins/portal-vue", ssr: false },
    // { src: "~/plugins/hammer-directive", ssr: false },
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/tailwindcss',
    '@aceforth/nuxt-optimized-images',
  ],
  /*
   ** AceForth Image Optimizer
   */
  optimizedImages: {
    optimizeImages: true
  },
  /*
   ** SVG Loader
   */
  svgLoader: {
    svgoConfig: {
      plugins: [
        { prefixIds: false } // Disables prefixing for SVG IDs
      ]
    }
  },
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    'nuxt-client-init-module',
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/prismic',
    '@nuxtjs/style-resources',
    'nuxt-polyfill',
    'nuxt-svg-loader',
    'portal-vue/nuxt',

    ['vue-scrollto/nuxt', {
      container: '.app-inner',
      duration: 500,
      easing: 'ease',
      offset: 0,
      force: true,
      cancelable: true,
      onStart: function(element) {
        if(process.env.NODE_ENV !== 'production'){
          console.log(`vue-scrollto: start`);
        }
      },
      onDone: function(element) {
        if(process.env.NODE_ENV !== 'production'){
          console.log(`vue-scrollto: done`);
        }
      },
      onCancel: function(element) {
        if(process.env.NODE_ENV !== 'production'){
          console.log(`vue-scrollto: cancel`);
        }
      },
  
      x: false,
      y: true
    }],
  ],
  /*
   ** Prismic config
   */	
  prismic: {
    endpoint: prismic.endpoint,
    linkResolver: '~/plugins/link-resolver',
    modern: true,
  },

  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},

  /*
   ** Tailwind configuration
   ** See https://tailwindcss.com/docs/
   */
  tailwindcss: {
    // Options
    cssPath: '~/tailwind/tailwind.scss',
    configPath: '~/tailwind/tailwind.config.js',
  },

  /*
   ** Babel presets
   */
  babel: {
    presets: [
      [
        "@babel/preset-env",
        {
          useBuiltIns: "corejs",

          // List assembled with
          // https://browserl.ist/?q=%3E+1%25+in+NL%2C+iOS+%3E%3D+10%2C+Safari+%3E%3D+10%2C+Firefox+ESR%2C+not+IE+%3C%3D+10%2C+not+ExplorerMobile+%3E+0%2C+not+BlackBerry+%3E+0%2C+not+OperaMini+all%2C+not+OperaMobile+%3E+0%2C+not+dead
          // for > ~90% coverage in the Netherlands
          targets: pkg.browserslist,
        },
      ],
    ],
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */

    postcss: {
      // Add plugin names as key and arguments as value
      // Install them before as dependencies with npm or yarn
      plugins: {
        "postcss-normalize": {
          browsers: pkg.browserslist,
        },
        "postcss-preset-env": {
          browsers: pkg.browserslist,
        },
        doiuse: {
          browsers: pkg.browserslist,
          ignore: [
            "object-fit",
            "calc",
            "css-clip-path",
            "css-masks",
            "css-appearance",
            "css-featurequeries",
            "css-filters",
            "css-gradients",
            "css-initial-value",
            "css-resize",
            "css-touch-action",
            "css-unset-value",
            "css3-cursors",
            "css3-cursors-newer",
            "flexbox",
            "intrinsic-width",
            "outline",
            "pointer",
            "text-size-adjust",
            "viewport-units",
            "wordwrap",
          ],
        },
      },
      preset: {
        // Change the postcss-preset-env settings
        autoprefixer: {
          grid: false,
        },
      },
    },

    loaders: {
      vue: {
        transformAssetUrls: {
          audio: 'src'
        }
      }
    },
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/,
          options: {
            fix: true
          }
        });
      }

      // Load audio
      config.module.rules.push({
        test: /\.(ogg|mp3|wav|mpe?g)$/i,
        loader: "file-loader",
        options: {
          name: "[path][name].[ext]",
          esModule: false,
        },
      });

      // Log checklist
      logProjectInfo();
      if(process.env.NODE_ENV === 'production'){

        // Build iNos iframe link
        const results = replace.sync({
          files: 'static/link.html',
          from: [/op3\/vue-nuxt-starter/g,'NOS op 3 Interactive | NOS op 3'],
          to: [meta.sysName,meta.title],
        });
        console.log('Updated link.html:', results);
      }
    }
  },

  // Server config
  // Enable to use instead ip adress instead of localhost
  // This might be useful for mobile developing
  
  server: {       
    host: '0.0.0.0',
  },
}
