# op3-nuxt-tailwind-template

> A project boilerplate template for future interactives using the javascript framework Nuxt.js and TailwindCSS


## Before starting a new project

1. Clone or fork this starter template and make a _new_ git repository. Make sure the git remotes point to a new and clean repository on bitbucket.

2. Before starting a new project, make sure you setup the project by running the command below in bash. The bash script will fill out the necessary information in package.json and project-settings.json in the root folder. You can always edit the settings afterwards.

        bash bash/setup.sh

## Project-settings.json

After finishing the `setup.bash` script. Check the values in `project-settings.json`. This file will automatically configure/update the settings in nuxt config and other components. 

See below for an explanation:

1. ### [Meta]:
    #### sysName <**string**>

        op3/dev-nuxt-tailwindwind-template

    #### name <**string**>

        "project-pkg-name-lowercase-with-no-spaces-illegal-characters"

    #### title <**string**>:

        NOS op 3 Interactive | NOS op 3

    #### description <**string**>:

        Ontdek een interactief verhaal van NOS op 3!

    #### twitterHashtags <**string**>:

        #interactive #nosop3

    #### hasPrettyUrl <**boolean**>:

        true || false

    #### prettyUrl <**String**>:

        http://nosop3.nl/[project]

    
2. ### [Prismic]:

3. ### [Tracking]:

    #### path <**string**>:

    Contains `name` (reponame), year and month.

        app.op3-nuxt-template.2020.01

4. ### [Enalyzer]:
    #### enalyzerCode <**string**>:

    Make a new survey on [Enalyzer](https://www.enalyzer.com) and get a code for the feedback button. Account can be provided by NOS Digitale Media.

        ENALYZER_ID

5. ### [Deploy]:

    #### isProd <**boolean**>:

        true || false
    #### showLogs <**boolean**>:

        true || false
    
    #### showTrackingLogs <**boolean**>:

        true || false



## Creating components

There are many commonly used components already available in the `~/components/` directory. Using them is not required at all, but they can be very helpful.


## Icons

## Content Management

### Prismic
*TODO*

### Static folder

- Text content is generally placed in JSON's in the `/data/` directory. See the README.md there for more information about content.

- Images, audio, video and such are either loaded as minified assets through webpack in the `/assets/` directory in the appropriate folders.

- Multimedia that should not be handled by webpack should be placed somewhere in the `/static/` directory.


## Finishing & Deployment

### Checklist
Complete the following steps shortly before shipping to production:

- Check that the title, system name, description and enalyzer code are replaced everywhere correctly.

- Check that the correct static images are included:

    - A "nos-website.jpg" in `~/static/img/` with a corresponding reference to it in `~/static/link.html`.

    - A "share.jpg" in `~/static/img/` with a corresponding reference to it in the metadata in `nuxt.config.js`.

- Swap the tracking ID in `nuxt.config.js` to switch from NPO dev to NPO prod (only do this when you really are no longer developing and testing)

- Instruct the editor to link to `https://app.nos.nl/op3/SYSNAME/link.html` in iNOS. This redirects the iframe link.

### Deployment
NOS op 3 heeft een test- en een productieomgeving:

- http://test.app.nos.nl - test.nos-shell1.omroep.nl.

  Let op: deze omgeving is geen vergaarbak voor allerhande testjes. Als je tijdelijk iets wil uitproberen is dat prima, maar verwijder daarna wel je bestanden en/of mappen.

- https://app.nos.nl - nos-shell1.omroep.nl


### Folders and directories 

To keep files and folders organised, it is important follow the structure below. This is less important for the test environment. Please delete projects and/or experiments that are not being used anymore to keep the server neat and tidy.

```
pages/
│
└── op3/
    ├── interactive-1/
    │   ├── app.js
    │   ├── index.html
    │   └── styles.css
    ├── interactive-2/
    ├── interactive-3/
    └── quiz/
        ├── quiz-1/
        │    ├── app.js
        │    ├── index.html
        │    └── styles.css
        └── quiz-2/
```

New project directories are to be created in `/e/ap/app.nos.nl/pages/op3`.


## Tailwind

### Setup
*TODO*

### Config viewer
*TODO*

### Deploy
*TODO*



## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).